#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 1024

char g_username[50];
char g_password[50];
int g_flag;
char g_database[50];

void send_request(int sock, int flag, const char *username, const char *password, const char *database, const char *command) {
    char buffer[BUFFER_SIZE];
    sprintf(buffer, "%d %s %s %s %s", flag, username, password, database, command);
    send(sock, buffer, strlen(buffer), 0);
}

void handle_login(int sock, const char *username, const char *password) {
    send_request(sock, 0, username, password, "", "login");
    
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    recv(sock, buffer, sizeof(buffer), 0);
    
    printf("Server response: %s\n", buffer);
    
    if (strcmp(buffer, "Authentication successful") == 0) {
        strcpy(g_username, username);
        strcpy(g_password, password);
    } else if (strcmp(buffer, "Authentication failed") == 0) {
        printf("Login failed. Exiting...\n");
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    int flag = 0;
    char username[50];
    char password[50];
    char command[50];
    
    if (getuid() == 0) {
        strcpy(username, "root");
        strcpy(password, "root");
    } else if (argc == 5) {
        if (strcmp(argv[1], "-u") == 0 && strcmp(argv[3], "-p") == 0) {
            strcpy(username, argv[2]);
            strcpy(password, argv[4]);
        }
    } else {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        return 1;
    }
    
    printf("Username: %s\n", username);
    printf("Password: %s\n", password);
    
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(12345);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    
    if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Connection failed");
        return 1;
    }
    
    handle_login(sock, username, password);
    
    while (1) {
        printf("Enter command (or 'exit' to quit): ");
        fgets(command, sizeof(command), stdin);
        command[strcspn(command, "\n")] = '\0';  
        
        if (strcmp(command, "exit") == 0) {
            break;
        }
        
        send_request(sock, flag, g_username, g_password, g_database, command);
        
      
        char buffer[BUFFER_SIZE];
        memset(buffer, 0, sizeof(buffer));
        int recv_result = recv(sock, buffer, sizeof(buffer), 0);
        if (recv_result <= 0) {
           
            printf("Server disconnected. Exiting...\n");
            break;
        }
        
        printf("Server response: %s\n", buffer);
     
        if (strstr(buffer, "Access to database '") != NULL && strstr(buffer, "' granted") != NULL) {
            sscanf(buffer, "Access to database '%[^']'", g_database);
            flag = 1;
        }
    }
    
    close(sock);
    
    return 0;
}
