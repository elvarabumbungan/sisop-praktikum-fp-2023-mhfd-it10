# sisop-praktikum-fp-2023-MHFD-IT10


# Database

 __Analisa Soal__


Pada soal disuruh membuat progam database menggunakan bahasa c, dalam progam database tersebut harus bisa membuat database, membuat table, menghapus database, menghapus tabel, menghapus kolom, menambahkan data pada kolom, mengubah data pada kolom, menghapus data pada kolom, dan memilih kolom.


__Code Progam__

- **Client**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 1024

char g_username[50];
char g_password[50];
int g_flag;
char g_database[50];

void send_request(int sock, int flag, const char *username, const char *password, const char *database, const char *command) {
    char buffer[BUFFER_SIZE];
    sprintf(buffer, "%d %s %s %s %s", flag, username, password, database, command);
    send(sock, buffer, strlen(buffer), 0);
}

void handle_login(int sock, const char *username, const char *password) {
    send_request(sock, 0, username, password, "", "login");
    
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    recv(sock, buffer, sizeof(buffer), 0);
    
    printf("Server response: %s\n", buffer);
    
    if (strcmp(buffer, "Authentication successful") == 0) {
        strcpy(g_username, username);
        strcpy(g_password, password);
    } else if (strcmp(buffer, "Authentication failed") == 0) {
        printf("Login failed. Exiting...\n");
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    int flag = 0;
    char username[50];
    char password[50];
    char command[50];
    
    if (getuid() == 0) {
        strcpy(username, "root");
        strcpy(password, "root");
    } else if (argc == 5) {
        if (strcmp(argv[1], "-u") == 0 && strcmp(argv[3], "-p") == 0) {
            strcpy(username, argv[2]);
            strcpy(password, argv[4]);
        }
    } else {
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        return 1;
    }
    
    printf("Username: %s\n", username);
    printf("Password: %s\n", password);
    
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(12345);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    
    if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Connection failed");
        return 1;
    }
    
    handle_login(sock, username, password);
    
    while (1) {
        printf("Enter command (or 'exit' to quit): ");
        fgets(command, sizeof(command), stdin);
        command[strcspn(command, "\n")] = '\0';  
        
        if (strcmp(command, "exit") == 0) {
            break;
        }
        
        send_request(sock, flag, g_username, g_password, g_database, command);
        
      
        char buffer[BUFFER_SIZE];
        memset(buffer, 0, sizeof(buffer));
        int recv_result = recv(sock, buffer, sizeof(buffer), 0);
        if (recv_result <= 0) {
           
            printf("Server disconnected. Exiting...\n");
            break;
        }
        
        printf("Server response: %s\n", buffer);
     
        if (strstr(buffer, "Access to database '") != NULL && strstr(buffer, "' granted") != NULL) {
            sscanf(buffer, "Access to database '%[^']'", g_database);
            flag = 1;
        }
    }
    
    close(sock);
    
    return 0;
}
```
Berhubungan dengan server dengan socket, saat pertama membuka akan otomatis command login, jika jalam dengan sudo akan login dengan usernam dan password root. flag berubah jadi 1 saat diberikan akses ke database dan menambahkan database di request.  

- **Authenticate**
```
void authenticate(int sock, const char *username, const char *password) {
    char db_path[] = "databases/users/users.csv";
    FILE *db = fopen(db_path, "r");
    
    if (db == NULL) {
        send(sock, "Database not found", 18, 0);
        return;
    }
    
    char line[BUFFER_SIZE];
    int found = 0;
    
    // Baca header file CSV
    fgets(line, sizeof(line), db);
    
    while (fgets(line, sizeof(line), db) != NULL) {
        line[strcspn(line, "\n")] = '\0';  // Remove newline character
        
        char id_str[50];
        char user[50];
        char pass[50];
        
        sscanf(line, "%[^,],%[^,],%s", id_str, user, pass);
        
        if (strcmp(user, username) == 0 && strcmp(pass, password) == 0) {
            found = 1;
            break;
        }
    }
    
    fclose(db);
    
    if (found || (strcmp("root", username) == 0 && strcmp("root", password) == 0)) {
        send(sock, "Authentication successful", 25, 0);
    } else {
        send(sock, "Authentication failed", 21, 0);
    }
}
```
Mengecek apakah username dan password sudah sesuai dengan data yang ada di database. kemudian mengirim balik response. digunakan saat login
- **handle_login**
```
void handle_login(int sock, const char *username, const char *password) {
    authenticate(sock, username, password);
}
```
Digunakan saat menerima command login
- **Handle_create_user**
```
void handle_create_user(int sock, const char *command, const char *username, const char *password) {
    // Hanya pengguna dengan username "root" yang dapat membuat pengguna baru
    // Syntax command: CREATE USER [nama_user] IDENTIFIED BY [password_user]
    char new_username[50];
    char new_password[50];
    
    if (sscanf(command, "CREATE USER %s IDENTIFIED BY %s", new_username, new_password) == 2) {
        if (strcmp(username, "root") != 0 && strcmp(password, "root") != 0) {
            send(sock, "Permission denied", 17, 0);
            return;
        }
        
        char db_path[] = "databases/users/users.csv";
        
        // Periksa apakah username sudah ada
        if (is_username_exist(db_path, new_username)) {
            send(sock, "Username already exists", 23, 0);
            return;
        }
        
        // Mencari idUser terakhir
        int last_id = get_last_id(db_path);
        
        if (last_id == -1) {
            send(sock, "Error opening database", 22, 0);
            return;
        }
        
        // Membuat idUser baru dengan id yang naik
        int new_id = last_id + 1;
        
        // Menambahkan pengguna baru ke file CSV
        FILE *db = fopen(db_path, "a");
        
        if (db == NULL) {
            send(sock, "Error opening database", 22, 0);
            return;
        }
        
        fprintf(db, "%d,%s,%s\n", new_id, new_username, new_password);
        
        fclose(db);
        
        send(sock, "User created successfully", 25, 0);
    } else {
        send(sock, "Invalid command create", 23, 0);
    }
}
```
Dipanggil saat command create user, hanya root yang bisa menggunakan.
- **handle_use_database**
```
void handle_use_database(int sock, const char *command, const char *username) {
    // Syntax command: USE [nama_database];
    char database_name[50];
    
    if (sscanf(command, "USE %[^;];", database_name) == 1) {
        char users_db_path[] = "databases/users/users.csv";
        char databases_db_path[] = "databases/users/databases.csv";
        char akses_db_path[] = "databases/users/akses.csv";
        
        
        // Jika pengguna adalah 'root', berikan akses
        if (strcmp(username, "root") == 0) {
            char response[50];
            sprintf(response, "Access to database '%s' granted", database_name);
            send(sock, response, strlen(response), 0);
            return;
        }
        
        // Baca idUser dari pengguna yang sedang login
        int id_user = get_user_id(users_db_path, username);
        if (id_user == -1) {
            send(sock, "User not found", 14, 0);
            return;
        }
        
        // Baca idDb dari nama database yang dimasukkan
        int id_db = get_database_id(databases_db_path, database_name);
        if (id_db == -1) {
            send(sock, "Database not found", 18, 0);
            return;
        }
        
        // Periksa akses pengguna ke database
        bool access_granted = check_access(akses_db_path, id_user, id_db);
        if (access_granted) {
            char response[50];
            sprintf(response, "Access to database '%s' granted", database_name);
            send(sock, response, strlen(response), 0);
        } else {
            send(sock, "Access denied", 13, 0);
        }
    } else {
        send(sock, "Invalid command", 16, 0);
    }
}
```
Use database, mengecek terlebih dahulu apakah ada akses, dengan mengambil id user dan id database kemudian cek di table akses

- **handle_grant_permission**
```
void handle_grant_permission(int sock, const char *command, const char *username) {
    // Syntax command: GRANT PERMISSION [nama_database] INTO [nama_user];
    char database_name[50];
    char user_to_grant[50];

    if (sscanf(command, "GRANT PERMISSION %[^ ] INTO %[^;];", database_name, user_to_grant) == 2) {
        char users_db_path[] = "databases/users/users.csv";
        char databases_db_path[] = "databases/users/databases.csv";
        char akses_db_path[] = "databases/users/akses.csv";

        // Jika pengguna bukan 'root', berikan akses
        if (strcmp(username, "root") != 0) {
            send(sock, "Permission denied", 17, 0);
            return;
        }

        // Baca idDb dari nama database yang dimasukkan
        int id_db = get_database_id(databases_db_path, database_name);
        if (id_db == -1) {
            send(sock, "Database not found", 18, 0);
            return;
        }

        // Baca idUser dari nama user yang dimasukkan
        int id_user_to_grant = get_user_id(users_db_path, user_to_grant);
        if (id_user_to_grant == -1) {
            send(sock, "User to grant not found", 23, 0);
            return;
        }

        // Periksa akses pengguna ke database
        bool access_granted = check_access(akses_db_path, id_user_to_grant, id_db);
        if (access_granted) {
            send(sock, "Access already granted", 22, 0);
            return;
        }

        // Menambahkan akses pengguna ke database
        FILE *akses_db = fopen(akses_db_path, "a");
        if (akses_db == NULL) {
            send(sock, "Error opening akses database", 28, 0);
            return;
        }

        fprintf(akses_db, "%d,%d\n", id_user_to_grant, id_db);

        fclose(akses_db);

        send(sock, "Permission granted successfully", 30, 0);
    } else {
        send(sock, "Invalid command", 16, 0);
    }
}
```
Hanya bisa diakses oleh root, mengambil id user dan database lalu menambahkan data di table akses

- **handle_create_database**
```
void handle_create_database(int sock, const char *command, const char *username) {
    // Syntax command: CREATE DATABASE [nama_database];
    char database_name[50];

    if (sscanf(command, "CREATE DATABASE %[^;];", database_name) == 1) {
        char users_db_path[] = "databases/users/users.csv";
        char databases_db_path[] = "databases/users/databases.csv";
        char akses_db_path[] = "databases/users/akses.csv";

        // Baca idUser dari pengguna yang sedang login
        int id_user = get_user_id(users_db_path, username);
        if (id_user == -1 && strcmp(username, "root")) {
            send(sock, "User not found", 14, 0);
            return;
        }

        bool is_root = (strcmp(username, "root") == 0);

        // Periksa apakah database sudah ada
        int id_Db = get_database_id(databases_db_path, database_name);
        if (id_Db != -1) {
            send(sock, "Database already exists", 23, 0);
            return;
        }

        // Membuat direktori database
        char database_path[100];
        sprintf(database_path, "databases/%s", database_name);
        if (mkdir(database_path, 0777) != 0) {
            send(sock, "Error creating database", 23, 0);
            return;
        }

        // Mendapatkan idDb berikutnya
        int id_db = get_last_id(databases_db_path);
        id_db +=  1;

        // Menambahkan informasi database ke databases.csv
        FILE *databases_db = fopen(databases_db_path, "a");
        if (databases_db == NULL) {
            send(sock, "Error opening databases database", 33, 0);
            return;
        }

        fprintf(databases_db, "%d,%s\n", id_db, database_name);

        fclose(databases_db);

        // Jika bukan root, tambahkan akses pengguna ke database
        if (!is_root) {
            FILE *akses_db = fopen(akses_db_path, "a");
            if (akses_db == NULL) {
                send(sock, "Error opening akses database", 28, 0);
                return;
            }

            fprintf(akses_db, "%d,%d\n", id_user, id_db);

            fclose(akses_db);
        }

        // Panggil fungsi USE DATABASE untuk mengaktifkan database yang baru dibuat
        char reqUse[50];
        sprintf(reqUse, "USE %s;", database_name);
        handle_use_database(sock, reqUse, username);
    } else {
        send(sock, "Invalid command", 16, 0);
    }
}
```
Membuat direktori baru, lalu tambahkan data database ke table database, lalu tambahkan akses sesuai id user dan id database. kemudian otomatis menggunakan use database saat berhasil

- **drop_database**
```
void drop_database(const char *database_name) {
    // Remove database directory
    char database_path[100];
    sprintf(database_path, "databases/%s", database_name);
    
    if (rmdir(database_path) != 0) {
        printf("Failed to drop database '%s'\n", database_name);
    } else {
        printf("Database '%s' dropped successfully\n", database_name);
    }
}

void remove_from_file(const char *filename, const char *value) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error opening file '%s'\n", filename);
        return;
    }

    FILE *temp_file = fopen("temp.csv", "w");
    if (temp_file == NULL) {
        printf("Error creating temporary file\n");
        fclose(file);
        return;
    }

    char line[BUFFER_SIZE];
    fgets(line, sizeof(line), file); // Copy header line to temporary file
    fputs(line, temp_file);

    while (fgets(line, sizeof(line), file) != NULL) {
        line[strcspn(line, "\n")] = '\0';
        char *token = strtok(line, ",");
        if (token != NULL) {
            if (strcmp(token, value) != 0) {
                fputs(line, temp_file);
                fputs("\n", temp_file);
            }
        }
    }

    fclose(file);
    fclose(temp_file);

    if (remove(filename) != 0) {
        printf("Error removing file '%s'\n", filename);
        return;
    }

    if (rename("temp.csv", filename) != 0) {
        printf("Error renaming file\n");
    }
}

void drop_database_access(const char *username, const char *database_name) {
    // Remove user's access to the database from akses.csv file
    char access_file[50];
    sprintf(access_file, "databases/users/akses.csv");
    remove_from_file(access_file, database_name);
}

void drop_database_entry(const char *database_name) {
    // Remove database entry from databases.csv file
    remove_from_file("databases/users/databases.csv", database_name);
}

int check_database_access(const char *username, const char *database_name) {
    // Check if the user has access to the database
    if(!strcmp(username, "root")) return 1;
    
    char access_file[50];
    sprintf(access_file, "databases/users/akses.csv");
    
    FILE *file = fopen(access_file, "r");
    if (file == NULL) {
        printf("Error opening file '%s'\n", access_file);
        return 0;
    }
    
    char line[BUFFER_SIZE];
    while (fgets(line, sizeof(line), file) != NULL) {
        line[strcspn(line, "\n")] = '\0';
        if (strcmp(line, database_name) == 0) {
            fclose(file);
            return 1;
        }
    }
    
    fclose(file);
    return 0;
}

void handle_drop_database(int sock, const char *command, const char *username) {
    char database_name[50];

    if (sscanf(command, "DROP DATABASE %[^;];", database_name) == 1) {
        // Check if the user has access to the database
        if (check_database_access(username, database_name)) {
            drop_database(database_name);
            drop_database_access(username, database_name);
            drop_database_entry(database_name);
            send(sock, "Database dropped successfully", 29, 0);
        } else {
            send(sock, "Access denied", 14, 0);
        }
    } else {
        send(sock, "Invalid command format", 22, 0);
    }
}
```
Menghapus direktori, data di table akses dan database

- **handle_request**
```
void handle_request(int sock, const char *request) {
    int flag;
    char command[50];
    char username[20];
    char password[20];
    char database[20];

    sscanf(request, "%d", &flag);
    
    if (flag == 1) {
        sscanf(request, "%d %s %s %s %[^\n]", &flag, username, password, database, command);
        printf("Received request:\n");
        printf("Flag: %d\n", flag);
        printf("Username: %s\n", username);
        printf("Password: %s\n", password);
        printf("Database: %s\n", database);
        printf("Command: %s\n", command);
    } else if (flag == 0) {
        sscanf(request, "%d %s %s %[^\n]", &flag, username, password, command);
        printf("Received request:\n");
        printf("Flag: %d\n", flag);
        printf("Username: %s\n", username);
        printf("Password: %s\n", password);
        printf("Command: %s\n", command);

    }

    if (strcmp(command, "login") == 0) {
        handle_login(sock, username, password);
    } else if (strncmp(command, "CREATE USER", 11) == 0) {
        size_t command_len = strlen(command);
        if (command[command_len - 1] == ';') {
            command[command_len - 1] = '\0';  // Menghapus karakter ';'
            handle_create_user(sock, command, username, password);
        } else {
            send(sock, "Invalid command", 16, 0);
        }
    } else if (strncmp(command, "USE ", 4) == 0) {
        handle_use_database(sock, command, username);
    } else if (strncmp(command, "GRANT PERMISSION", 16) == 0) {
        handle_grant_permission(sock, command, username);
    } else if (strncmp(command, "CREATE DATABASE", 15) == 0) {
        handle_create_database(sock, command, username);
    } else if (strncmp(command, "DROP DATABASE", 13) == 0) {
        handle_drop_database(sock, command, username);
    } else if (strncmp(command, "CREATE TABLE", 12) == 0 && flag) {
        char *database_Name_ = database;
        char table_Name_[100];
        char _Columns[100];
        sscanf(command, "CREATE TABLE %s %[^;];", table_Name_, _Columns);
        MembuatTable(sock, database_Name_, table_Name_, _Columns);
    } else if (strncmp(command, "DROP TABLE", 10) == 0 && flag) {
        char *database_Name_ = database;
        char *table_Name_[100];
        sscanf(command, "DROP TABLE %[^;];", table_Name_);
        MenghapusTable(sock, database_Name_, table_Name_);
    } else if (strncmp(command, "DROP COLUMN", 11) == 0 && flag) {
        char *database_Name_ = database;
        char table_Name_[100];
        char Column__Name[100];
        sscanf(command, "DROP COLUMN %s FROM %[^;];", table_Name_, Column__Name);
        MenghapusKolom(sock, database_Name_, table_Name_, Column__Name);
    } else if (strncmp(command, "INSERT", 6) == 0 && flag) {
        char *database_Name_ = database;
        char *table_Name_[100];
        char *values[100];
        sscanf(command, "INSERT INTO %s %[^;];", table_Name_, values);
        MenambahData(sock, database_Name_, table_Name_, values);
    } else if (strncmp(command, "UPDATE", 6) == 0 && flag) {
        char *database_Name_ = database;
        char *table_Name_[100];
        char *Column__Name[100];
        char *New__Data[100];
        sscanf(command, "UPDATE %s SET %[^=]=%[^';'];", table_Name_, Column__Name, New__Data);
        MengubahData(sock, database_Name_, table_Name_, Column__Name, New__Data);
    } else if (strncmp(command, "DELETE", 6) == 0) {
        char *database_Name_ = database;
        char *table_Name_ = strtok(NULL, "\n");
        sscanf(command, "DELETE FROM %[^;];", table_Name_);
        HapusData(sock, database_Name_, table_Name_);
    }
    else {
        send(sock, "Invalid command", 16, 0);
    }
}

```
Membaca request sesuai format. dengan tanda jika flag=0 maka belum akses database, jika flag=1 sudah akses database/

- **handle_client**
```
void *handle_client(void *arg) {
    int client_sock = *(int *)arg;
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    
    while (recv(client_sock, buffer, sizeof(buffer), 0) > 0) {
        printf("Received request from client %d\n", client_sock);
        printf("Request: %s\n", buffer);
        
        handle_request(client_sock, buffer);
        
        memset(buffer, 0, sizeof(buffer));
    }
    
    close(client_sock);
    free(arg);
    pthread_exit(NULL);
}

int main() {
    int server_sock = socket(AF_INET, SOCK_STREAM, 0);
    
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(12345);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    
    if (bind(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Binding failed");
        return 1;
    }
    
    listen(server_sock, 5);
    printf("Server listening on port 12345...\n");
    
    while (1) {
        struct sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        
        int *client_sock = malloc(sizeof(int));
        *client_sock = accept(server_sock, (struct sockaddr *)&client_addr, &client_len);
        if (*client_sock < 0) {
            perror("Accept failed");
            free(client_sock);
            continue;
        }
        
        printf("Client connected: %d\n", *client_sock);
        
        pthread_t tid;
        pthread_create(&tid, NULL, handle_client, (void *)client_sock);
        pthread_detach(tid);
    }
    
    close(server_sock);
    
    return 0;
}
```
Untuk melakukan koneksi socket dengan client
- **Membuat Tabel**
```
void MembuatTable(int sock, char *database_Name_, char *table_Name_, char *_Columns) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);
    char Database_Location[512];
    snprintf(Database_Location, sizeof(Database_Location), "databases/%s", database_Name_);

    struct stat st;
    if (stat(Database_Location, &st) != 0 || !S_ISDIR(st.st_mode)) {
        printf("Database '%s' not found.\n", database_Name_);
        send(sock, "Database not found", strlen("Database not found"), 0);
    }

    FILE *file = fopen(File_Location, "w");
    if (file == NULL) {
        printf("Failed to create table '%s'.\n", table_Name_);
        return;
    }

    for (int i = 0; table_Name_[i] != '\0'; i++) {
        if (!isalnum(table_Name_[i])) {
            printf("Invalid table name.\n");
            send(sock, "Invalid table name", strlen("Invalid table name"), 0);
            fclose(file);
            return;
        }
    }

    int len = strlen(_Columns);
    _Columns[len - 2] = '\0';

    fprintf(file, "%s\n", _Columns);

    fclose(file);

    printf("Table '%s' was created successfully in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "Table was created successfully", strlen("Table was created successfully"), 0);
}

```
1. Fungsi **`MembuatTable`** menerima argumen untuk membuat tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel dan lokasi database.
3. Fungsi **`stat`** digunakan untuk memeriksa ketersediaan direktori database.
4. Fungsi **`fopen`** digunakan untuk membuka file tabel dengan mode "w" (write).
5. Dilakukan pengecekan karakter-karakter dalam nama tabel, jika tidak valid, maka akan dicetak pesan error.
6. Panjang daftar kolom dihitung dan formatnya disesuaikan.
7. Daftar kolom ditulis ke dalam file tabel menggunakan **`fprintf`**.
8. File tabel ditutup menggunakan **`fclose`**.
9. Pesan sukses dicetak dan pesan sukses dikirimkan ke soket.

- **Menghapus tabel**
```
void MenghapusTable(int sock, char *database_Name_, char *table_Name_) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }

    int status = remove(File_Location);

    if (status == 0) {
        printf("Table '%s' was successfully removed from database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table was successfully removed", strlen("Table was successfully removed"), 0);
    } else {
        printf("Failed to delete table '%s' from database '%s'.\n", table_Name_, database_Name_);
    }
}
```
1. Fungsi **`MenghapusTable`** menerima argumen untuk menghapus sebuah tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. Fungsi **`remove`** digunakan untuk menghapus file tabel.
5. Jika penghapusan berhasil, dicetak pesan sukses.
6. Jika penghapusan gagal, dicetak pesan error.

- **Menghapus kolom**
```
void MenghapusKolom(int sock, char *database_Name_, char *table_Name_, char *Column__Name) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }
    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }
    char line_[500];
    fgets(line_, sizeof(line_), file);

    int _Index__Column = -1;
    char *__token = strtok(line_, ",");
    int _Index = 0;
    while (__token != NULL) {
        if (strcmp(__token, Column__Name) == 0) {
            _Index__Column = _Index;
            break;
        }
        _Index++;
        __token = strtok(NULL, ",");
    }

    fclose(file);

    if (_Index__Column == -1) {
        printf("Column '%s' not found in table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
        send(sock, "Column not found", strlen("Column not found"), 0);
        return;
    }

    char File_Temporary_Location[512];
    snprintf(File_Temporary_Location, sizeof(File_Temporary_Location), "databases/%s/temp.csv", database_Name_);
    FILE *File_Temporary = fopen(File_Temporary_Location, "w");
    if (File_Temporary == NULL) {
        printf("Failed to create temporary file.\n");
        return;
    }

    file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        fclose(File_Temporary);
        remove(File_Temporary_Location);
        return;
    }

    while (fgets(line_, sizeof(line_), file)) {
        char *__token = strtok(line_, ",");
        int columnCount = 0;
        while (__token != NULL) {
            columnCount++;
            if (columnCount != _Index__Column + 1) {
                fprintf(File_Temporary, "%s,", __token);
            }
            __token = strtok(NULL, ",");
        }
        fprintf(File_Temporary, "\n");
    }

    fclose(file);
    fclose(File_Temporary);

    remove(File_Location);

    rename(File_Temporary_Location, File_Location);

    printf("Column '%s' was successfully deleted from table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
    send(sock, "Column was successfully deleted", strlen("Column was successfully deleted"), 0);
}

```
1. Fungsi **`MenghapusKolom`** menerima argumen untuk menghapus sebuah kolom dalam sebuah tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "r" (read) menggunakan **`fopen`**.
5. Baris pertama file tabel dibaca dan disimpan dalam variabel.
6. Kolom yang ingin dihapus diidentifikasi berdasarkan nama kolom yang cocok dengan baris pertama.
7. File tabel ditutup menggunakan **`fclose`**.
8. Jika kolom tidak ditemukan, dicetak pesan error.
9. File temporary baru dibuat.
10. File tabel dibuka kembali dan file temporary dibuka. Jika gagal, dicetak pesan error.
11. Baris-baris file tabel diproses, kolom yang ingin dihapus tidak ditulis ke file temporary.
12. Setelah semua baris selesai diproses, file tabel dan file temporary ditutup.
13. File tabel asli dihapus dan file temporary diubah namanya menjadi file tabel asli.
14. Pesan sukses dicetak dan pesan sukses dikirimkan ke soket.

- **Menambah data**
```
void MenambahData(int sock, char *database_Name_, char *table_Name_, char *values) {

    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }

    FILE *file = fopen(File_Location, "a");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char *__token = strtok(values, ",");
    char line_[500] = "";
    bool Value1 = true;

    while (__token != NULL) {
       
        while (*__token == ' ' || *__token == '(' || *__token == '\'')
            __token++;
        size_t len = strlen(__token);
        while (len > 0 && (__token[len - 1] == ' ' || __token[len - 1] == ')' || __token[len - 1] == '\''))
            __token[--len] = '\0';

    
        if (Value1) {
            Value1 = false;
            snprintf(line_ + strlen(line_), sizeof(line_) - strlen(line_), "%s", __token);
        } else {
            snprintf(line_ + strlen(line_), sizeof(line_) - strlen(line_), ",%s", __token);
        }

        __token = strtok(NULL, ",");
    }

    fprintf(file, "%s\n", line_);
    fclose(file);

    printf("Data added successfully to table '%s' in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "Data added successfully to table", strlen("Data added successfully to table"), 0);
}
```
1. Fungsi **`MenambahData`** menerima argumen untuk menambahkan data ke dalam sebuah tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "a" (append) menggunakan **`fopen`**.
5. Data yang akan ditambahkan diproses dalam loop untuk menghilangkan karakter yang tidak diinginkan.
6. Setiap token data yang telah diproses ditambahkan ke variabel, dipisahkan oleh tanda koma.
7. Data yang akan ditambahkan ditulis ke file tabel menggunakan **`fprintf`**.
8. File tabel ditutup menggunakan **`fclose`**.
9. Pesan sukses dicetak.

- **Mengubah data**
```
void MengubahData(int sock, char *database_Name_, char *table_Name_, char *Column__Name, char *New__Data) {
   
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);


    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }


    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char line_[500];
    fgets(line_, sizeof(line_), file);

    int _Index__Column = -1;
    char *__token = strtok(line_, ",");
    int _Index = 0;
    while (__token != NULL) {
        if (strcmp(__token, Column__Name) == 0) {
            _Index__Column = _Index;
            break;
        }
        _Index++;
        __token = strtok(NULL, ",");
    }

    fclose(file);

    if (_Index__Column == -1) {
        printf("Column '%s' not found in table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
        send(sock, "Column not found", strlen("Column not found"), 0);
        return;
    }

    char File_Temporary_Location[512];
    snprintf(File_Temporary_Location, sizeof(File_Temporary_Location), "databases/%s/temp.csv", database_Name_);
    FILE *File_Temporary = fopen(File_Temporary_Location, "w");
    if (File_Temporary == NULL) {
        printf("Failed to create temporary file.\n");
        return;
    }

    file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        fclose(File_Temporary);
        remove(File_Temporary_Location);
        return;
    }

    fgets(line_, sizeof(line_), file);
    fprintf(File_Temporary, "%s", line_);

    while (fgets(line_, sizeof(line_), file)) {
        char *__token = strtok(line_, ",");
        int columnCount = 0;
        while (__token != NULL) {
            columnCount++;
            if (columnCount == _Index__Column + 1) {
                fprintf(File_Temporary, "%s,", New__Data);
            } else {
                fprintf(File_Temporary, "%s,", __token);
            }
            __token = strtok(NULL, ",");
        }
        fprintf(File_Temporary, "\n");
    }

    fclose(file);
    fclose(File_Temporary);


    remove(File_Location);

    rename(File_Temporary_Location, File_Location);

    printf("Column '%s' in table '%s' in database '%s' was successfully updated.\n", Column__Name, table_Name_, database_Name_);
    send(sock, "Column was successfully updated", strlen("Column was successfully updated"), 0);
}
```
1. Fungsi **`MengubahData`** menerima argumen untuk mengubah data dalam sebuah kolom dalam tabel sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "r" (read) menggunakan **`fopen`**.
5. Baris pertama file tabel dibaca untuk mendapatkan nama-nama kolom.
6. Kolom yang ingin diubah dicari dalam baris pertama tersebut.
7. File tabel ditutup.
8. Jika kolom tidak ditemukan, dicetak pesan error.
9. File temporary dibuat untuk menyimpan hasil perubahan.
10. File tabel dibuka kembali.
11. Baris pertama file tabel ditulis ke file temporary.
12. Setiap baris dari file tabel dibaca dan diproses.
13. Setiap token dalam baris diproses. Jika di kolom yang ingin diubah, data baru ditulis ke file temporary. Jika tidak, token tersebut ditulis kembali.
14. Setelah semua token dalam baris selesai diproses, baris tersebut ditulis ke file temporary.
15. Setelah semua baris selesai diproses, file tabel dan file temporary ditutup.
16. File tabel asli dihapus.
17. File temporary diubah namanya menjadi nama file tabel asli.
18. Pesan sukses dicetak.

- **Menghapus data**
```
void HapusData(int sock, char *database_Name_, char *table_Name_) {
  
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }


    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char line_[500];
    fgets(line_, sizeof(line_), file);
    
    fclose(file);

    file = fopen(File_Location, "w");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    fprintf(file, "%s", line_);
    
    fclose(file);

    printf("All rows have been deleted from table '%s' in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "All rows have been deleted from table", strlen("All rows have been deleted from table"), 0);
}

```
1. Fungsi **`HapusData`** menerima argumen untuk menghapus semua data dalam sebuah tabel dalam database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "r" (read) menggunakan **`fopen`**.
5. Baris pertama file tabel dibaca untuk mendapatkan nama-nama kolom.
6. File tabel ditutup.
7. File tabel dibuka kembali dalam mode "w" (write) menggunakan **`fopen`**.
8. Baris pertama (nama-nama kolom) ditulis kembali ke file tabel.
9. File tabel ditutup kembali.
10. Pesan sukses dicetak.

__Output__
![create user](https://i.ibb.co/stQHJJQ/create-user.jpg)
![create database](https://i.ibb.co/kJpxcPm/create-database.jpg)
![use access deniend](https://i.ibb.co/kc84Y7L/use-acces-denied.jpg)
![Grant permission](https://i.ibb.co/bXsBJtd/grant-permission.jpg)
![use access granted](https://i.ibb.co/gVv6PPr/use-acces-granted.jpg)
![Insert](https://i.ibb.co/cy6F4vP/insert-into.jpg)
![Update](https://i.ibb.co/Thj76j1/update-table.jpg)
![drop kolom](https://i.ibb.co/NNSWQyQ/drop-kolom.jpg)
![delete from](https://i.ibb.co/zSjbVKm/delete-from.jpg)
![Drop table](https://i.ibb.co/n3r2WV3/drop-table.jpg)
__Kendala__

Belum selesai DML dan susah untuk membuat progam yang melacak file csv.
