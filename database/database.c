#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BUFFER_SIZE 1024

void authenticate(int sock, const char *username, const char *password) {
    char db_path[] = "databases/users/users.csv";
    FILE *db = fopen(db_path, "r");
    
    if (db == NULL) {
        send(sock, "Database not found", 18, 0);
        return;
    }
    
    char line[BUFFER_SIZE];
    int found = 0;
    
    // Baca header file CSV
    fgets(line, sizeof(line), db);
    
    while (fgets(line, sizeof(line), db) != NULL) {
        line[strcspn(line, "\n")] = '\0';  // Remove newline character
        
        char id_str[50];
        char user[50];
        char pass[50];
        
        sscanf(line, "%[^,],%[^,],%s", id_str, user, pass);
        
        if (strcmp(user, username) == 0 && strcmp(pass, password) == 0) {
            found = 1;
            break;
        }
    }
    
    fclose(db);
    
    if (found || (strcmp("root", username) == 0 && strcmp("root", password) == 0)) {
        send(sock, "Authentication successful", 25, 0);
    } else {
        send(sock, "Authentication failed", 21, 0);
    }
}

void handle_login(int sock, const char *username, const char *password) {
    authenticate(sock, username, password);
}

int get_last_id(const char *db_path) {
    FILE *db = fopen(db_path, "r");
    
    if (db == NULL) {
        return -1; // Error opening database
    }
    
    int last_id = 0;
    char line[BUFFER_SIZE];
    
    while (fgets(line, sizeof(line), db) != NULL) {
        int id;
        sscanf(line, "%d,", &id);
        
        if (id > last_id) {
            last_id = id;
        }
    }
    
    fclose(db);
    
    return last_id;
}

bool is_username_exist(const char *db_path, const char *username) {
    FILE *db = fopen(db_path, "r");
    
    if (db == NULL) {
        return false;  // Error opening database
    }
    
    char line[BUFFER_SIZE];
    
    while (fgets(line, sizeof(line), db) != NULL) {
        char stored_username[50];
        sscanf(line, "%*d,%[^,]", stored_username);
        
        if (strcmp(stored_username, username) == 0) {
            fclose(db);
            return true;  // Username already exists
        }
    }
    
    fclose(db);
    
    return false;  // Username does not exist
}

void handle_create_user(int sock, const char *command, const char *username, const char *password) {
    // Hanya pengguna dengan username "root" yang dapat membuat pengguna baru
    // Syntax command: CREATE USER [nama_user] IDENTIFIED BY [password_user]
    char new_username[50];
    char new_password[50];
    
    if (sscanf(command, "CREATE USER %s IDENTIFIED BY %s", new_username, new_password) == 2) {
        if (strcmp(username, "root") != 0 && strcmp(password, "root") != 0) {
            send(sock, "Permission denied", 17, 0);
            return;
        }
        
        char db_path[] = "databases/users/users.csv";
        
        // Periksa apakah username sudah ada
        if (is_username_exist(db_path, new_username)) {
            send(sock, "Username already exists", 23, 0);
            return;
        }
        
        // Mencari idUser terakhir
        int last_id = get_last_id(db_path);
        
        if (last_id == -1) {
            send(sock, "Error opening database", 22, 0);
            return;
        }
        
        // Membuat idUser baru dengan id yang naik
        int new_id = last_id + 1;
        
        // Menambahkan pengguna baru ke file CSV
        FILE *db = fopen(db_path, "a");
        
        if (db == NULL) {
            send(sock, "Error opening database", 22, 0);
            return;
        }
        
        fprintf(db, "%d,%s,%s\n", new_id, new_username, new_password);
        
        fclose(db);
        
        send(sock, "User created successfully", 25, 0);
    } else {
        send(sock, "Invalid command create", 23, 0);
    }
}

int get_user_id(const char *users_db_path, const char *username) {
    FILE *users_db = fopen(users_db_path, "r");
    
    if (users_db == NULL) {
        return -1; // Error opening users database
    }
    
    char line[BUFFER_SIZE];
    
    // Skip header line
    fgets(line, sizeof(line), users_db);
    
    while (fgets(line, sizeof(line), users_db) != NULL) {
        char id_str[50];
        char user[50];
        sscanf(line, "%[^,],%[^,],%*s", id_str, user);
        
        if (strcmp(user, username) == 0) {
            int id_user = atoi(id_str);
            fclose(users_db);
            return id_user;
        }
    }
    
    fclose(users_db);
    
    return -1; // User not found
}

int get_database_id(const char *databases_db_path, const char *database_name) {
    FILE *databases_db = fopen(databases_db_path, "r");

    if (databases_db == NULL) {
        return -1; // Error opening databases database
    }

    char line[BUFFER_SIZE];

    // Skip header line
    fgets(line, sizeof(line), databases_db);

    while (fgets(line, sizeof(line), databases_db) != NULL) {
        char id_str[50];
        char name[50];
        sscanf(line, "%[^,],%[^,\n]", id_str, name);

        if (strcmp(name, database_name) == 0) {
            int id_db = atoi(id_str);
            fclose(databases_db);
            return id_db;
        }
    }

    fclose(databases_db);

    return -1; // Database not found
}

bool check_access(const char *akses_db_path, int id_user, int id_db) {
    FILE *akses_db = fopen(akses_db_path, "r");
    
    if (akses_db == NULL) {
        return false; // Error opening akses database
    }
    
    char line[BUFFER_SIZE];
    
    // Skip header line
    fgets(line, sizeof(line), akses_db);
    
    while (fgets(line, sizeof(line), akses_db) != NULL) {
        int id_user_db, id_db_db;
        sscanf(line, "%d,%d", &id_user_db, &id_db_db);
        
        if (id_user_db == id_user && id_db_db == id_db) {
            fclose(akses_db);
            return true; // Access granted
        }
    }
    
    fclose(akses_db);
    
    return false; // Access denied
}

void handle_use_database(int sock, const char *command, const char *username) {
    // Syntax command: USE [nama_database];
    char database_name[50];
    
    if (sscanf(command, "USE %[^;];", database_name) == 1) {
        char users_db_path[] = "databases/users/users.csv";
        char databases_db_path[] = "databases/users/databases.csv";
        char akses_db_path[] = "databases/users/akses.csv";
        
        
        // Jika pengguna adalah 'root', berikan akses
        if (strcmp(username, "root") == 0) {
            char response[50];
            sprintf(response, "Access to database '%s' granted", database_name);
            send(sock, response, strlen(response), 0);
            return;
        }
        
        // Baca idUser dari pengguna yang sedang login
        int id_user = get_user_id(users_db_path, username);
        if (id_user == -1) {
            send(sock, "User not found", 14, 0);
            return;
        }
        
        // Baca idDb dari nama database yang dimasukkan
        int id_db = get_database_id(databases_db_path, database_name);
        if (id_db == -1) {
            send(sock, "Database not found", 18, 0);
            return;
        }
        
        // Periksa akses pengguna ke database
        bool access_granted = check_access(akses_db_path, id_user, id_db);
        if (access_granted) {
            char response[50];
            sprintf(response, "Access to database '%s' granted", database_name);
            send(sock, response, strlen(response), 0);
        } else {
            send(sock, "Access denied", 13, 0);
        }
    } else {
        send(sock, "Invalid command", 16, 0);
    }
}

void handle_grant_permission(int sock, const char *command, const char *username) {
    // Syntax command: GRANT PERMISSION [nama_database] INTO [nama_user];
    char database_name[50];
    char user_to_grant[50];

    if (sscanf(command, "GRANT PERMISSION %[^ ] INTO %[^;];", database_name, user_to_grant) == 2) {
        char users_db_path[] = "databases/users/users.csv";
        char databases_db_path[] = "databases/users/databases.csv";
        char akses_db_path[] = "databases/users/akses.csv";

        // Jika pengguna bukan 'root', berikan akses
        if (strcmp(username, "root") != 0) {
            send(sock, "Permission denied", 17, 0);
            return;
        }

        // Baca idDb dari nama database yang dimasukkan
        int id_db = get_database_id(databases_db_path, database_name);
        if (id_db == -1) {
            send(sock, "Database not found", 18, 0);
            return;
        }

        // Baca idUser dari nama user yang dimasukkan
        int id_user_to_grant = get_user_id(users_db_path, user_to_grant);
        if (id_user_to_grant == -1) {
            send(sock, "User to grant not found", 23, 0);
            return;
        }

        // Periksa akses pengguna ke database
        bool access_granted = check_access(akses_db_path, id_user_to_grant, id_db);
        if (access_granted) {
            send(sock, "Access already granted", 22, 0);
            return;
        }

        // Menambahkan akses pengguna ke database
        FILE *akses_db = fopen(akses_db_path, "a");
        if (akses_db == NULL) {
            send(sock, "Error opening akses database", 28, 0);
            return;
        }

        fprintf(akses_db, "%d,%d\n", id_user_to_grant, id_db);

        fclose(akses_db);

        send(sock, "Permission granted successfully", 30, 0);
    } else {
        send(sock, "Invalid command", 16, 0);
    }
}

void handle_create_database(int sock, const char *command, const char *username) {
    // Syntax command: CREATE DATABASE [nama_database];
    char database_name[50];

    if (sscanf(command, "CREATE DATABASE %[^;];", database_name) == 1) {
        char users_db_path[] = "databases/users/users.csv";
        char databases_db_path[] = "databases/users/databases.csv";
        char akses_db_path[] = "databases/users/akses.csv";

        // Baca idUser dari pengguna yang sedang login
        int id_user = get_user_id(users_db_path, username);
        if (id_user == -1 && strcmp(username, "root")) {
            send(sock, "User not found", 14, 0);
            return;
        }

        bool is_root = (strcmp(username, "root") == 0);

        // Periksa apakah database sudah ada
        int id_Db = get_database_id(databases_db_path, database_name);
        if (id_Db != -1) {
            send(sock, "Database already exists", 23, 0);
            return;
        }

        // Membuat direktori database
        char database_path[100];
        sprintf(database_path, "databases/%s", database_name);
        if (mkdir(database_path, 0777) != 0) {
            send(sock, "Error creating database", 23, 0);
            return;
        }

        // Mendapatkan idDb berikutnya
        int id_db = get_last_id(databases_db_path);
        id_db +=  1;

        // Menambahkan informasi database ke databases.csv
        FILE *databases_db = fopen(databases_db_path, "a");
        if (databases_db == NULL) {
            send(sock, "Error opening databases database", 33, 0);
            return;
        }

        fprintf(databases_db, "%d,%s\n", id_db, database_name);

        fclose(databases_db);

        // Jika bukan root, tambahkan akses pengguna ke database
        if (!is_root) {
            FILE *akses_db = fopen(akses_db_path, "a");
            if (akses_db == NULL) {
                send(sock, "Error opening akses database", 28, 0);
                return;
            }

            fprintf(akses_db, "%d,%d\n", id_user, id_db);

            fclose(akses_db);
        }

        // Panggil fungsi USE DATABASE untuk mengaktifkan database yang baru dibuat
        char reqUse[50];
        sprintf(reqUse, "USE %s;", database_name);
        handle_use_database(sock, reqUse, username);
    } else {
        send(sock, "Invalid command", 16, 0);
    }
}

void drop_database(const char *database_name) {
    // Remove database directory
    char database_path[100];
    sprintf(database_path, "databases/%s", database_name);
    
    if (rmdir(database_path) != 0) {
        printf("Failed to drop database '%s'\n", database_name);
    } else {
        printf("Database '%s' dropped successfully\n", database_name);
    }
}

void remove_from_file(const char *filename, const char *value) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error opening file '%s'\n", filename);
        return;
    }

    FILE *temp_file = fopen("temp.csv", "w");
    if (temp_file == NULL) {
        printf("Error creating temporary file\n");
        fclose(file);
        return;
    }

    char line[BUFFER_SIZE];
    fgets(line, sizeof(line), file); // Copy header line to temporary file
    fputs(line, temp_file);

    while (fgets(line, sizeof(line), file) != NULL) {
        line[strcspn(line, "\n")] = '\0';
        char *token = strtok(line, ",");
        if (token != NULL) {
            if (strcmp(token, value) != 0) {
                fputs(line, temp_file);
                fputs("\n", temp_file);
            }
        }
    }

    fclose(file);
    fclose(temp_file);

    if (remove(filename) != 0) {
        printf("Error removing file '%s'\n", filename);
        return;
    }

    if (rename("temp.csv", filename) != 0) {
        printf("Error renaming file\n");
    }
}

void drop_database_access(const char *username, const char *database_name) {
    // Remove user's access to the database from akses.csv file
    char access_file[50];
    sprintf(access_file, "databases/users/akses.csv");
    remove_from_file(access_file, database_name);
}

void drop_database_entry(const char *database_name) {
    // Remove database entry from databases.csv file
    remove_from_file("databases/users/databases.csv", database_name);
}

int check_database_access(const char *username, const char *database_name) {
    // Check if the user has access to the database
    if(!strcmp(username, "root")) return 1;
    
    char access_file[50];
    sprintf(access_file, "databases/users/akses.csv");
    
    FILE *file = fopen(access_file, "r");
    if (file == NULL) {
        printf("Error opening file '%s'\n", access_file);
        return 0;
    }
    
    char line[BUFFER_SIZE];
    while (fgets(line, sizeof(line), file) != NULL) {
        line[strcspn(line, "\n")] = '\0';
        if (strcmp(line, database_name) == 0) {
            fclose(file);
            return 1;
        }
    }
    
    fclose(file);
    return 0;
}

void handle_drop_database(int sock, const char *command, const char *username) {
    char database_name[50];

    if (sscanf(command, "DROP DATABASE %[^;];", database_name) == 1) {
        // Check if the user has access to the database
        if (check_database_access(username, database_name)) {
            drop_database(database_name);
            drop_database_access(username, database_name);
            drop_database_entry(database_name);
            send(sock, "Database dropped successfully", 29, 0);
        } else {
            send(sock, "Access denied", 14, 0);
        }
    } else {
        send(sock, "Invalid command format", 22, 0);
    }
}

void MembuatTable(int sock, char *database_Name_, char *table_Name_, char *_Columns) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);
    char Database_Location[512];
    snprintf(Database_Location, sizeof(Database_Location), "databases/%s", database_Name_);

    struct stat st;
    if (stat(Database_Location, &st) != 0 || !S_ISDIR(st.st_mode)) {
        printf("Database '%s' not found.\n", database_Name_);
        send(sock, "Database not found", strlen("Database not found"), 0);
    }

    FILE *file = fopen(File_Location, "w");
    if (file == NULL) {
        printf("Failed to create table '%s'.\n", table_Name_);
        return;
    }

    for (int i = 0; table_Name_[i] != '\0'; i++) {
        if (!isalnum(table_Name_[i])) {
            printf("Invalid table name.\n");
            send(sock, "Invalid table name", strlen("Invalid table name"), 0);
            fclose(file);
            return;
        }
    }

    int len = strlen(_Columns);
    _Columns[len - 2] = '\0';

    fprintf(file, "%s\n", _Columns);

    fclose(file);

    printf("Table '%s' was created successfully in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "Table was created successfully", strlen("Table was created successfully"), 0);
}

void MenghapusTable(int sock, char *database_Name_, char *table_Name_) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }

    int status = remove(File_Location);

    if (status == 0) {
        printf("Table '%s' was successfully removed from database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table was successfully removed", strlen("Table was successfully removed"), 0);
    } else {
        printf("Failed to delete table '%s' from database '%s'.\n", table_Name_, database_Name_);
    }
}

void MenghapusKolom(int sock, char *database_Name_, char *table_Name_, char *Column__Name) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }
    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }
    char line_[500];
    fgets(line_, sizeof(line_), file);

    int _Index__Column = -1;
    char *__token = strtok(line_, ",");
    int _Index = 0;
    while (__token != NULL) {
        if (strcmp(__token, Column__Name) == 0) {
            _Index__Column = _Index;
            break;
        }
        _Index++;
        __token = strtok(NULL, ",");
    }

    fclose(file);

    if (_Index__Column == -1) {
        printf("Column '%s' not found in table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
        send(sock, "Column not found", strlen("Column not found"), 0);
        return;
    }

    char File_Temporary_Location[512];
    snprintf(File_Temporary_Location, sizeof(File_Temporary_Location), "databases/%s/temp.csv", database_Name_);
    FILE *File_Temporary = fopen(File_Temporary_Location, "w");
    if (File_Temporary == NULL) {
        printf("Failed to create temporary file.\n");
        return;
    }

    file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        fclose(File_Temporary);
        remove(File_Temporary_Location);
        return;
    }

    while (fgets(line_, sizeof(line_), file)) {
        char *__token = strtok(line_, ",");
        int columnCount = 0;
        while (__token != NULL) {
            columnCount++;
            if (columnCount != _Index__Column + 1) {
                fprintf(File_Temporary, "%s,", __token);
            }
            __token = strtok(NULL, ",");
        }
        fprintf(File_Temporary, "\n");
    }

    fclose(file);
    fclose(File_Temporary);

    remove(File_Location);

    rename(File_Temporary_Location, File_Location);

    printf("Column '%s' was successfully deleted from table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
    send(sock, "Column was successfully deleted", strlen("Column was successfully deleted"), 0);
}

void MenambahData(int sock, char *database_Name_, char *table_Name_, char *values) {

    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }

    FILE *file = fopen(File_Location, "a");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char *__token = strtok(values, ",");
    char line_[500] = "";
    bool Value1 = true;

    while (__token != NULL) {
       
        while (*__token == ' ' || *__token == '(' || *__token == '\'')
            __token++;
        size_t len = strlen(__token);
        while (len > 0 && (__token[len - 1] == ' ' || __token[len - 1] == ')' || __token[len - 1] == '\''))
            __token[--len] = '\0';

    
        if (Value1) {
            Value1 = false;
            snprintf(line_ + strlen(line_), sizeof(line_) - strlen(line_), "%s", __token);
        } else {
            snprintf(line_ + strlen(line_), sizeof(line_) - strlen(line_), ",%s", __token);
        }

        __token = strtok(NULL, ",");
    }

    fprintf(file, "%s\n", line_);
    fclose(file);

    printf("Data added successfully to table '%s' in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "Data added successfully to table", strlen("Data added successfully to table"), 0);
}

void MengubahData(int sock, char *database_Name_, char *table_Name_, char *Column__Name, char *New__Data) {
   
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);


    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }


    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char line_[500];
    fgets(line_, sizeof(line_), file);

    int _Index__Column = -1;
    char *__token = strtok(line_, ",");
    int _Index = 0;
    while (__token != NULL) {
        if (strcmp(__token, Column__Name) == 0) {
            _Index__Column = _Index;
            break;
        }
        _Index++;
        __token = strtok(NULL, ",");
    }

    fclose(file);

    if (_Index__Column == -1) {
        printf("Column '%s' not found in table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
        send(sock, "Column not found", strlen("Column not found"), 0);
        return;
    }

    char File_Temporary_Location[512];
    snprintf(File_Temporary_Location, sizeof(File_Temporary_Location), "databases/%s/temp.csv", database_Name_);
    FILE *File_Temporary = fopen(File_Temporary_Location, "w");
    if (File_Temporary == NULL) {
        printf("Failed to create temporary file.\n");
        return;
    }

    file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        fclose(File_Temporary);
        remove(File_Temporary_Location);
        return;
    }

    fgets(line_, sizeof(line_), file);
    fprintf(File_Temporary, "%s", line_);

    while (fgets(line_, sizeof(line_), file)) {
        char *__token = strtok(line_, ",");
        int columnCount = 0;
        while (__token != NULL) {
            columnCount++;
            if (columnCount == _Index__Column + 1) {
                fprintf(File_Temporary, "%s,", New__Data);
            } else {
                fprintf(File_Temporary, "%s,", __token);
            }
            __token = strtok(NULL, ",");
        }
        fprintf(File_Temporary, "\n");
    }

    fclose(file);
    fclose(File_Temporary);


    remove(File_Location);

    rename(File_Temporary_Location, File_Location);

    printf("Column '%s' in table '%s' in database '%s' was successfully updated.\n", Column__Name, table_Name_, database_Name_);
    send(sock, "Column was successfully updated", strlen("Column was successfully updated"), 0);
}

void HapusData(int sock, char *database_Name_, char *table_Name_) {
  
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }


    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char line_[500];
    fgets(line_, sizeof(line_), file);
    
    fclose(file);

    file = fopen(File_Location, "w");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    fprintf(file, "%s", line_);
    
    fclose(file);

    printf("All rows have been deleted from table '%s' in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "All rows have been deleted from table", strlen("All rows have been deleted from table"), 0);
}

void handle_request(int sock, const char *request) {
    int flag;
    char command[50];
    char username[20];
    char password[20];
    char database[20];

    sscanf(request, "%d", &flag);
    
    if (flag == 1) {
        sscanf(request, "%d %s %s %s %[^\n]", &flag, username, password, database, command);
        printf("Received request:\n");
        printf("Flag: %d\n", flag);
        printf("Username: %s\n", username);
        printf("Password: %s\n", password);
        printf("Database: %s\n", database);
        printf("Command: %s\n", command);
    } else if (flag == 0) {
        sscanf(request, "%d %s %s %[^\n]", &flag, username, password, command);
        printf("Received request:\n");
        printf("Flag: %d\n", flag);
        printf("Username: %s\n", username);
        printf("Password: %s\n", password);
        printf("Command: %s\n", command);

    }

    if (strcmp(command, "login") == 0) {
        handle_login(sock, username, password);
    } else if (strncmp(command, "CREATE USER", 11) == 0) {
        size_t command_len = strlen(command);
        if (command[command_len - 1] == ';') {
            command[command_len - 1] = '\0';  // Menghapus karakter ';'
            handle_create_user(sock, command, username, password);
        } else {
            send(sock, "Invalid command", 16, 0);
        }
    } else if (strncmp(command, "USE ", 4) == 0) {
        handle_use_database(sock, command, username);
    } else if (strncmp(command, "GRANT PERMISSION", 16) == 0) {
        handle_grant_permission(sock, command, username);
    } else if (strncmp(command, "CREATE DATABASE", 15) == 0) {
        handle_create_database(sock, command, username);
    } else if (strncmp(command, "DROP DATABASE", 13) == 0) {
        handle_drop_database(sock, command, username);
    } else if (strncmp(command, "CREATE TABLE", 12) == 0 && flag) {
        char *database_Name_ = database;
        char table_Name_[100];
        char _Columns[100];
        sscanf(command, "CREATE TABLE %s %[^;];", table_Name_, _Columns);
        MembuatTable(sock, database_Name_, table_Name_, _Columns);
    } else if (strncmp(command, "DROP TABLE", 10) == 0 && flag) {
        char *database_Name_ = database;
        char *table_Name_[100];
        sscanf(command, "DROP TABLE %[^;];", table_Name_);
        MenghapusTable(sock, database_Name_, table_Name_);
    } else if (strncmp(command, "DROP COLUMN", 11) == 0 && flag) {
        char *database_Name_ = database;
        char table_Name_[100];
        char Column__Name[100];
        sscanf(command, "DROP COLUMN %s FROM %[^;];", table_Name_, Column__Name);
        MenghapusKolom(sock, database_Name_, table_Name_, Column__Name);
    } else if (strncmp(command, "INSERT", 6) == 0 && flag) {
        char *database_Name_ = database;
        char *table_Name_[100];
        char *values[100];
        sscanf(command, "INSERT INTO %s %[^;];", table_Name_, values);
        MenambahData(sock, database_Name_, table_Name_, values);
    } else if (strncmp(command, "UPDATE", 6) == 0 && flag) {
        char *database_Name_ = database;
        char *table_Name_[100];
        char *Column__Name[100];
        char *New__Data[100];
        sscanf(command, "UPDATE %s SET %[^=]=%[^';'];", table_Name_, Column__Name, New__Data);
        MengubahData(sock, database_Name_, table_Name_, Column__Name, New__Data);
    } else if (strncmp(command, "DELETE", 6) == 0) {
        char *database_Name_ = database;
        char *table_Name_ = strtok(NULL, "\n");
        sscanf(command, "DELETE FROM %[^;];", table_Name_);
        HapusData(sock, database_Name_, table_Name_);
    }
    else {
        send(sock, "Invalid command", 16, 0);
    }
}

void *handle_client(void *arg) {
    int client_sock = *(int *)arg;
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    
    while (recv(client_sock, buffer, sizeof(buffer), 0) > 0) {
        printf("Received request from client %d\n", client_sock);
        printf("Request: %s\n", buffer);
        
        handle_request(client_sock, buffer);
        
        memset(buffer, 0, sizeof(buffer));
    }
    
    close(client_sock);
    free(arg);
    pthread_exit(NULL);
}

int main() {
    int server_sock = socket(AF_INET, SOCK_STREAM, 0);
    
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(12345);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    
    if (bind(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Binding failed");
        return 1;
    }
    
    listen(server_sock, 5);
    printf("Server listening on port 12345...\n");
    
    while (1) {
        struct sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        
        int *client_sock = malloc(sizeof(int));
        *client_sock = accept(server_sock, (struct sockaddr *)&client_addr, &client_len);
        if (*client_sock < 0) {
            perror("Accept failed");
            free(client_sock);
            continue;
        }
        
        printf("Client connected: %d\n", *client_sock);
        
        pthread_t tid;
        pthread_create(&tid, NULL, handle_client, (void *)client_sock);
        pthread_detach(tid);
    }
    
    close(server_sock);
    
    return 0;
}
